package kz.aitu.oop.examples.practicePatterns.practice7;

public class Pizza implements Food {

    @Override
    public String getType() {
        return "The factory returned class Pizza"+"\n"+"Someone ordered a fast food!";
    }
}
