package kz.aitu.oop.examples.practicePatterns.practice5;

public class PreciousStone {
    private EachPreciousStone ps;

    public PreciousStone(){
        ps = new EachPreciousStone();
    }

    public PreciousStone addId(){
        ps.setId(ps.getId() + 1);
        return this;
    }
    public PreciousStone addCost(){       //addCost
        ps.setPrecious_cost(ps.getPrecious_cost() + 7800);
        return this;
    }
    public PreciousStone addWeight(){
        ps.setPrecious_weight(ps.getPrecious_weight() + 4);   //addWeight
        return this;
    }
    public EachPreciousStone build(){
        return ps;
    }
}
