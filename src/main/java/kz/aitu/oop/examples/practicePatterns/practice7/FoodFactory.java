package kz.aitu.oop.examples.practicePatterns.practice7;

public class FoodFactory {

    public Food getFood(String s) {
        if (s.equals ("pizza")) {
            return new Pizza();
        }
        if (s.equals("cake")) {
            return new Cake();
        }
        return null;
    }
}
