package kz.aitu.oop.examples.practicePatterns.practice5;

import lombok.Data;

@Data
public class EachSemiPreciousStone {
    private int id;
    public double semi_cost;
    public String name;
    public int semi_weight;
}
