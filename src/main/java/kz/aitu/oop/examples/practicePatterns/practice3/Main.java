package kz.aitu.oop.examples.practicePatterns.practice3;

// FACTORY METHOD PATTERN

import java.util.LinkedList;

public class Main {
    public static void main(String[] args) {
        EmployeeInfo employee = new EmployeeInfo(1, "Yerkebulan", "Data Scientist", 10000);
        EmployeeInfo employee1 = new EmployeeInfo(2,"Yerzhan", "Data Engineer", 2000);
        EmployeeInfo employee2 = new EmployeeInfo(3,"Olki","Mafia Forever",15000);

        Project project = new Project(1, 200000);

        Company com = new Company();
        com.addEmployee(employee);
        com.addEmployee(employee1);
        com.addEmployee(employee2);
        com.addProject(project);
        System.out.println("Number of employees in current project: " + com.getNumberOfEmployees());

        com.getSalary();
        com.getProjectCost();
        com.finalCost();

        LinkedList<EmployeeInfo> emplist = new LinkedList<>();
        emplist.add(employee);
        LinkedList<Project> projects = new LinkedList<>();
        projects.add(project);


    }
}
