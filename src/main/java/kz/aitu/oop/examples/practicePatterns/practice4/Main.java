package kz.aitu.oop.examples.practicePatterns.practice4;

// BUILD METHOD PATTERN
import java.util.LinkedList;

public class Main {
    public static void main(String[] args) {
        Accessories accessories = new Accessories();
        EachAccessory accessory = accessories.addId().addName().addCost().build();
        System.out.println(accessory);

        Fishes fishes = new Fishes();
        EachFish fish = fishes.addId().addCost().build();
        System.out.println(fish);

        Reptiles reptiles = new Reptiles();
        EachReptile reptile = reptiles.addId().addCost().build();
        System.out.println(reptile);


        LinkedList<EachFish> f = new LinkedList<>();
        f.add(fish);
        LinkedList<EachAccessory> a = new LinkedList<>();
        a.add(accessory);
        LinkedList<EachReptile> r = new LinkedList<>();
        r.add(reptile);

        Aquarium aqua = new Aquarium();
        aqua.getTotalCost(f, r, a);
    }
}
