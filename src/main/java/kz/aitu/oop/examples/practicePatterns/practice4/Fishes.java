package kz.aitu.oop.examples.practicePatterns.practice4;

public class Fishes {
    private EachFish fish;

    public Fishes() {
        fish = new EachFish();
    }

    public Fishes addId() {
        fish.setId(fish.getId() + 1);       //build
        return this;
    }

    public Fishes addCost() {
        fish.setCost(fish.getCost() + 800);
        return this;
    }

    public EachFish build() {
        return fish;
    }

}
