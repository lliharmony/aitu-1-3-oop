package kz.aitu.oop.examples.practicePatterns.practice4;

import lombok.Data;

@Data
public class EachFish {
    private int id;
    public double cost;
}