package kz.aitu.oop.examples.practicePatterns.practice7;
// Pushpakov Yerkebulan
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        String food = input.next();

        FoodFactory fd = new FoodFactory();
        Food fo = fd.getFood(food);
        System.out.println(fo.getType());
    }
}
