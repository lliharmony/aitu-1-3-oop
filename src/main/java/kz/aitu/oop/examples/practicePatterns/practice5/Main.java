package kz.aitu.oop.examples.practicePatterns.practice5;

// BUILD METHOD PATTERN
import java.util.LinkedList;

public class Main {
    public static void main(String[] args) {
        PreciousStone precious = new PreciousStone();
        EachPreciousStone eachPreciousStone = precious
                .addId()
                .addCost()            // withBuild
                .addWeight()
                .build();

        SemiPreciousStone semi = new SemiPreciousStone();
        EachSemiPreciousStone eachSemiPreciousStone =  semi
                .addId()
                .addCost()
                .addWeight()
                .build();

        LinkedList<EachPreciousStone> eps = new LinkedList<>();
        eps.add(eachPreciousStone);
        LinkedList<EachSemiPreciousStone> esps = new LinkedList<>();
        esps.add(eachSemiPreciousStone);

        Stone stone = new Stone();
        stone.getTotalWeight(esps, eps);
        stone.getTotalCost(esps, eps);



    }
}
