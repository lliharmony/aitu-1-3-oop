package kz.aitu.oop.examples.practicePatterns.practice4;

import java.util.LinkedList;

public class Aquarium {
    public void getTotalCost(LinkedList<EachFish> fish, LinkedList<EachReptile> reptile, LinkedList<EachAccessory> accessory) {
        double total = 0;
        for(EachFish f : fish) total = total + f.getCost();
        for (EachReptile r : reptile) total = total + r.getCost();
        for (EachAccessory a : accessory) total = total + a.getCost();
        System.out.println(" ");
        System.out.println("Total price: " + total + "KZT");
    }
}
