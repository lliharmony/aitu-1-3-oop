package kz.aitu.oop.examples.practicePatterns.practice3;

import java.util.ArrayList;
import java.util.List;

public class Company implements FinalProject  {
    public List<EmployeeInfo> employeeInfos;
    public List<Project> project;

    public Company(){
        this.employeeInfos = new ArrayList<>();
        this.project = new ArrayList<>();
    }

    public int getNumberOfEmployees(){            //Number of employees
        return employeeInfos.size();
    }
    public void addEmployee(EmployeeInfo em){
        employeeInfos.add(em);
    }
    public void addProject(Project pr){
        project.add(pr);
    }


    @Override
    public void getSalary() {                     //getSalary
        double total = 0;
        for (EmployeeInfo emp : employeeInfos) total += emp.getSalary();
        System.out.println("Total salary: " + total + " KZT");
    }

    @Override
    public void getProjectCost() {
        double cost = 0;
        for (Project pro : project) cost += pro.getCost();
        System.out.println("Total cost of the current project is: " + cost + " KZT");
    }

    @Override
    public void finalCost() {
        double tootal = 0;
        for (EmployeeInfo emp : employeeInfos) tootal += emp.getSalary();
        for (Project pro : project) tootal += pro.getCost();
        System.out.println("Total Cost: " + tootal + " KZT");
    }
}
