package kz.aitu.oop.examples.practicePatterns.practice2;

public class WholeTrain {

    public void displayTotalNumberPassengers(RailwayCarriage passengers, Train total){
        System.out.println("Number of passengers in train: " + passengers.getNumberOfPassengers());
        System.out.println("Passengers: " + passengers.getPassengers());
        System.out.println("Total weight of both: " + total.TotalWeight()+"kg");
    }
}