package kz.aitu.oop.examples.practicePatterns.practice6;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        String s = input.nextLine();

        Singleton single = Singleton.getSingleInstance();
        single.str = s;

        Solution soo = new Solution();
        System.out.println(soo.checkString(single));

    }
}
