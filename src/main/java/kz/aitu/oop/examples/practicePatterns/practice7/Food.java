package kz.aitu.oop.examples.practicePatterns.practice7;

public interface Food {
    String getType();
}
