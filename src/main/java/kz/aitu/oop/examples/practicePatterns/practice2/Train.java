package kz.aitu.oop.examples.practicePatterns.practice2;

public class Train {
    public int TotalWeight() {
        return RailwayCarriage.getRailwayWeight() + Locomotive.getLocomotiveWeight();
    }
}
