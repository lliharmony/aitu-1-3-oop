package kz.aitu.oop.examples.practicePatterns.practice4;


public class Accessories {
    private EachAccessory accessory;

    public Accessories() {
        accessory = new EachAccessory();
    }

    public Accessories addId() {
        accessory.setId(accessory.getId() + 1);       //build
        return this;
    }

    public Accessories addName() {
        accessory.setName("Rakushka");
        return this;
    }

    public Accessories addCost() {
        accessory.setCost(accessory.getCost()+ 700);
        return this;
    }

    public EachAccessory build() {
        return accessory;
    }
}

