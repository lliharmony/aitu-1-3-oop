package kz.aitu.oop.examples.practicePatterns.practice2;

public class Passenger {
    public static int id;
    public String name;
    public String surname;

    private static Passenger instance = null;

    private Passenger() {
        id++;
    }
    public static Passenger getInstance(){
        if (instance == null) instance = new Passenger();
        return instance;
    }


    public void setId(int id) {
        this.id = id;
    }
    public void setName(String name) {
        this.name = name;
    }
    public void setSurname(String surname) {
        this.surname = surname;
    }

    @Override
    public String toString() {
        return "id = " + id +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                '}';
    }
}
