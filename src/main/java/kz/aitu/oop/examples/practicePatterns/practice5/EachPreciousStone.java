package kz.aitu.oop.examples.practicePatterns.practice5;

import lombok.Data;

@Data
public class EachPreciousStone {
    private int id;
    public double precious_cost;
    public String name;
    public int precious_weight;
}