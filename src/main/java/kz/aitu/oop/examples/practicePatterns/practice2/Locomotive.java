package kz.aitu.oop.examples.practicePatterns.practice2;


public class Locomotive {
    private static int id;
    public static int locomotiveWeight;

    private static Locomotive instance = null;

    private Locomotive() {
        id++;
    }
    public static Locomotive getInstance(){
        if (instance == null) instance = new Locomotive();
        return instance;
    }

    public static int getLocomotiveWeight(){
        return locomotiveWeight;
    }

    public static void setId(int id) {
        Locomotive.id = id;
    }

    public static void setLocomotiveWeight(int locomotiveWeight) {
        Locomotive.locomotiveWeight = locomotiveWeight;
    }

}
