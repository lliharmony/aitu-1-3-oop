package kz.aitu.oop.examples.practicePatterns.practice6;

import lombok.Data;

@Data
public class Singleton {
    private Singleton(){

    }
    public static String str;

    public static Singleton instance = null;

    public static Singleton getSingleInstance(){
        if (instance == null) instance = new Singleton();
        return instance;
    }

    @Override
    public String toString() {
        return str;
    }
}
