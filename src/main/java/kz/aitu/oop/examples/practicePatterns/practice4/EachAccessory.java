package kz.aitu.oop.examples.practicePatterns.practice4;

import lombok.Data;

@Data
public class EachAccessory {
    private int id;
    private String name;
    public double cost;
}
