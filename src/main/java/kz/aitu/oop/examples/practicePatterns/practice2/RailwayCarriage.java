package kz.aitu.oop.examples.practicePatterns.practice2;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class RailwayCarriage {
    private int id;
    public static int railwayWeight;
    public List<Passenger> passengers;

    public RailwayCarriage(int id, int railwayWeight) {
        this.id = id;
        this.railwayWeight = railwayWeight;
        this.passengers = new ArrayList<>();
    }
    public void addPassenger(Passenger passenger){       //addPassenger
        passengers.add(passenger);
    }

    public int getNumberOfPassengers(){
        return passengers.size();
    }
    public static int getRailwayWeight(){
        return railwayWeight;
    }
}