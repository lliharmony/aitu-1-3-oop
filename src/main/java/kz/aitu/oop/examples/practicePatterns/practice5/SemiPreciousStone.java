package kz.aitu.oop.examples.practicePatterns.practice5;

public class SemiPreciousStone {
    private EachSemiPreciousStone sps;

    public SemiPreciousStone(){
        sps = new EachSemiPreciousStone();
    }

    public SemiPreciousStone addId(){
        sps.setId(sps.getId() + 1);
        return this;
    }
    public SemiPreciousStone addCost(){       //addCost
        sps.setSemi_cost(sps.getSemi_cost() + 12000);
        return this;
    }
    public SemiPreciousStone addWeight(){
        sps.setSemi_weight(sps.getSemi_weight() + 7);    //addWeight
        return this;
    }
    public EachSemiPreciousStone build(){      //build
        return sps;
    }
}
