package kz.aitu.oop.examples.practicePatterns.practice3;

import lombok.Data;

@Data
public class Project {
    private int id;
    public double cost;

    public Project(int id, double cost) {
        this.id = id;
        this.cost = cost;
    }
}
