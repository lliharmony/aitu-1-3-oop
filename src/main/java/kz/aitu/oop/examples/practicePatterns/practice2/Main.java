package kz.aitu.oop.examples.practicePatterns.practice2;

// SINGLETONE METHOD PATTERN

public class Main {
    public static void main(String[] args) {
        Passenger passenger = Passenger.getInstance();
        passenger.setId(1);                           //singletone
        passenger.setName("Yerkebulan");
        passenger.setSurname("Pushpakov");

        Locomotive locomotive = Locomotive.getInstance();  //singletone
        locomotive.setId(100);
        locomotive.setLocomotiveWeight(15000);

        RailwayCarriage railwayCarriage = new RailwayCarriage(1,1200);
        railwayCarriage.addPassenger(passenger);

        Train train = new Train();
        train.TotalWeight();

        WholeTrain wholeTrain = new WholeTrain();
        wholeTrain.displayTotalNumberPassengers(railwayCarriage, train);
    }
}
