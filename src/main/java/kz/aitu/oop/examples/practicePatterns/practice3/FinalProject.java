package kz.aitu.oop.examples.practicePatterns.practice3;

public interface FinalProject {
    void getSalary();
    void getProjectCost();
    void finalCost();
}
