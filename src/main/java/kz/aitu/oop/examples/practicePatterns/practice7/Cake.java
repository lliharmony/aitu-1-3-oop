package kz.aitu.oop.examples.practicePatterns.practice7;

public class Cake implements Food {

    @Override
    public String getType() {
        return "The factory returned class Cake"+"\n"+"Someone ordered a dessert!";
    }
}
