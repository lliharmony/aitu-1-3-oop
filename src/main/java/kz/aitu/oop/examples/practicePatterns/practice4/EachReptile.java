package kz.aitu.oop.examples.practicePatterns.practice4;

import lombok.Data;

@Data
public class EachReptile {
    private int id;
    public double cost;
}
