package kz.aitu.oop.examples.practicePatterns.practice5;

import java.util.LinkedList;

public class Stone {
    public void getTotalWeight(LinkedList<EachSemiPreciousStone> SemiPreciousWeight, LinkedList<EachPreciousStone> preciousWeight) {
        int totalWeight = 0;
        for (EachSemiPreciousStone each : SemiPreciousWeight) totalWeight = totalWeight + each.getSemi_weight();
        for (EachPreciousStone eachP : preciousWeight) totalWeight = totalWeight + eachP.getPrecious_weight();
        System.out.println("Total weight: " + totalWeight + " carats");
    }

    public void getTotalCost(LinkedList<EachSemiPreciousStone> SemiPreciousCost, LinkedList<EachPreciousStone> preciousCost) {
        double total = 0;
        for (EachSemiPreciousStone each : SemiPreciousCost) total = total + each.getSemi_cost();
        for (EachPreciousStone eachP : preciousCost) total = total + eachP.getPrecious_cost();
        System.out.println("Total cost: " + total + " KZT");
    }
}