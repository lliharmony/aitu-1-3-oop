package kz.aitu.oop.examples.practicePatterns.practice6;

public class Solution {

    public String checkString(Singleton s) {
        if (s != null) {
            return "Hello I am a singletone! Let me say " + s.getSingleInstance() + " to you";
        }
        else
            return "ERROR";
    }
}
