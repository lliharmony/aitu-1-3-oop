package kz.aitu.oop.examples.practicePatterns.practice4;

public class Reptiles {
    private EachReptile reptile;

    public Reptiles() {
        reptile = new EachReptile();
    }

    public Reptiles addId() {
        reptile.setId(reptile.getId() + 1);       //build
        return this;
    }

    public Reptiles addCost() {
        reptile.setCost(reptile.getCost() + 1200);
        return this;
    }

    public EachReptile build() {
        return reptile;
    }
}
