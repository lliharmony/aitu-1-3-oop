package kz.aitu.oop.examples.subtask3;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/assignment")
public class Main {

    @GetMapping("subtask")
    public static void main(String[] args) {
        System.out.println("*******************");
        System.out.println("TestCircle:");
        Circle TestCircle = new Circle(1);
        System.out.println(TestCircle.toString());
        System.out.println("Perimeter: " + TestCircle.getPerimeter());
        System.out.println("Area: " + TestCircle.getArea());
        System.out.println("*******************");

        System.out.println( "TestResizableCircle:");
        ResizableCircle TestResizableCircle = new ResizableCircle(1);
        System.out.println("unResized radius: " + TestResizableCircle.toString());
        System.out.println("Resized radius: " + TestResizableCircle.resize());
        System.out.println("********************");
    }
}
