package kz.aitu.oop.examples.subtask3;

public interface GeometricObject {

    double getPerimeter();
    double getArea();
}
