package kz.aitu.oop.examples.subtask3;

public class Circle implements GeometricObject {
    protected double radius;

    public Circle(double radius) {
        this.radius = radius;
    }

    @Override
    public String toString() {
        return "Circle{" +
                "radius=" + radius +
                '}';
    }
    @Override
    public double getPerimeter(){
        return 3.14*2*radius;
    }
    @Override
    public double getArea(){
        return 3.14*radius*radius;
    }
}
