package kz.aitu.oop.examples.subtask3;

public class ResizableCircle extends Circle implements Resizable {

    public ResizableCircle(double radius) {
        super(radius);
    }

    @Override
    public String toString() {
        return "{" +
                "radius=" + radius +
                '}';
    }
    @Override
    public double resize() {
        return (radius*percent)/100;
    }
}
