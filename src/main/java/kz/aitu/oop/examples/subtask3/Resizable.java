package kz.aitu.oop.examples.subtask3;

public interface Resizable{
    int percent = 200;
    double resize();

}
