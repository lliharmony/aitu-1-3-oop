package kz.aitu.oop.examples.assignment7;

public class Rectangle extends Shape {

    protected double width;
    protected double length;

    public Rectangle(){
        super();
    }
    public Rectangle(double width, double length) {
        this.width = width;
        this.length = length;
    }

    public Rectangle(double width, double length, String color, boolean filled) {
        super(color, filled);
        this.width = width;
        this.length = length;
    }
    @Override

    public double getArea() {
        // A = width*length
        return width * length;
    }

    public double getPerimeter() {
        // P = 2(width+length)
        return 2 * (width + length);
    }

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public double getLength() {
        return length;
    }

    public void setLength(double length) {
        this.length = length;
    }
}