package kz.aitu.oop.examples.assignment7;

import org.springframework.web.bind.annotation.GetMapping;

public class Circle extends Shape {

    protected double radius;

    public Circle(){

    }

    public Circle(double radius) {
        this.radius = radius;
    }

    public Circle (double radius, String color, boolean filled) {
        super(color, filled);
        this.radius = radius;
    }
    @Override

    public double getArea(){
        //A= pi*r^2
        return 3.14*radius*radius;
    }
    public double getPerimeter(){
        // P = pi*2r
        return 3.14*2*radius;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }
}

