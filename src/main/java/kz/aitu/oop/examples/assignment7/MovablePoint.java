package kz.aitu.oop.examples.assignment7;

public class MovablePoint implements Movable {
    int x; //default ~
    int y;
    int xSpeed;
    int ySpeed;
    int t;

    public MovablePoint(int x, int y, int xSpeed, int ySpeed, int t) {
        this.x = x;
        this.y = y;
        this.xSpeed = xSpeed;
        this.ySpeed = ySpeed;
        this.t=t;
    }
    @Override
    public String toString() {
        return "MovablePoint{" +
                "x=" + x +
                ", y=" + y +
                ", xSpeed=" + xSpeed +
                ", ySpeed=" + ySpeed +
                '}';
    }


    @Override
    public void moveUp() {
        System.out.println(y+(ySpeed*t));
    }

    @Override
    public void moveDown() {
        System.out.println(y-(ySpeed*t));
    }

    @Override
    public void moveLeft() {
        System.out.println(x-(xSpeed*t));
    }

    @Override
    public void moveRight() {
        System.out.println(x+(xSpeed*t));
    }
}
