package kz.aitu.oop.examples.assignment7;

public class Square extends Rectangle{
    private double side;

    public Square(){
        super();
        double side = 1;
    }

    public Square(double side) {
        super(side,side);
        this.side = side;
    }
    public Square(double side,String color, boolean filled) {
        super(side,side,color, filled);
        this.side = side;
    }
    public double getSide() {
        return side;
    }

    public void setSide(double side) {
        this.side = side;
    }

    @Override

    public void setWidth(double side){
        System.out.println(side);
    }
    public void setLength(double side){
        System.out.println(side);
    }
}
