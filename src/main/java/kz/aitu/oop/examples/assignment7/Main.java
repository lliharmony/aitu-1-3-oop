package kz.aitu.oop.examples.assignment7;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


public class Main {

    public static void main(String[] args) {
        Shape s1 = new Circle(5.5, "red", false);
        System.out.println(s1);
        System.out.println(s1.getArea());
        System.out.println(s1.getPerimeter());
        System.out.println(s1.getColor());
        System.out.println(s1.isFilled());
        System.out.println(((Circle) s1).getRadius());

        System.out.println("******************");

        Circle c1 = (Circle) s1;
        System.out.println(c1);
        System.out.println(c1.getArea());
        System.out.println(c1.getPerimeter());
        System.out.println(c1.getColor());
        System.out.println(c1.isFilled());
        System.out.println(c1.getRadius());
        System.out.println("*************************");

        Shape s3 = new Rectangle(1, 2, "red", false);
        System.out.println(s3);
        System.out.println(s3.getArea());
        System.out.println(s3.getPerimeter());
        System.out.println(s3.getColor());
        System.out.println(((Rectangle) s3).getLength());
        System.out.println("****************");

        Rectangle r1 = (Rectangle) s3;
        System.out.println(r1);
        System.out.println(r1.getArea());
        System.out.println(r1.getPerimeter());
        System.out.println(r1.getColor());
        System.out.println(r1.getLength());
        System.out.println("******************");

        Shape s4 = new Square(6.6, "red", false);
        System.out.println(s4);
        System.out.println(s4.getArea());
        System.out.println(s4.getColor());
        System.out.println(((Square) s4).getSide());
        System.out.println("******************");

        Rectangle r2 = (Square) s4;
        System.out.println(r2);
        System.out.println(r2.getArea());
        System.out.println(r2.getColor());
        System.out.println(((Square) r2).getSide());
        System.out.println(r2.getLength());
        System.out.println("**********************");

        Square sq1 = (Square)r2;
        System.out.println(sq1);
        System.out.println(sq1.getArea());
        System.out.println(sq1.getColor());
        System.out.println(sq1.getSide());
        System.out.println(sq1.getLength());

        System.out.println("*********Subtask 2***********");

        MovablePoint Movable = new MovablePoint(30,23,4,5,4);
//Teacher, sorry I couldn't understand for what we need xSpeed and ySpeed. But to use them I found a displacement by the formula, x=x0+Vt(moving up,down,left,right)

        System.out.println("Initial place: "+ Movable.y + ", moving to the down at a speed of: "+ Movable.ySpeed + " and time: "+Movable.t);
        Movable.moveDown();
        System.out.println("Initial place: "+ Movable.y+", moving to the up at a speed of: "+ Movable.ySpeed + " and time: "+Movable.t);
        Movable.moveUp();
        System.out.println("Initial place: "+ Movable.x+", moving to the right at a speed of: "+ Movable.xSpeed + " and time: "+Movable.t);
        Movable.moveRight();
        System.out.println("Initial place: "+ Movable.x+", moving to the left at a speed of: "+ Movable.xSpeed + " and time: "+Movable.t);
        Movable.moveLeft();
    }
}


