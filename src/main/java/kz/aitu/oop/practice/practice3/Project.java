package kz.aitu.oop.practice.practice3;

import lombok.Data;

@Data
public class Project {
    private int id;
    public static double cost;

    public Project(int id, double cost) {
        this.id = id;
        this.cost = cost;
    }

    public static double getCost() {
        return cost;
    }
}
