package kz.aitu.oop.practice.practice3;

public class Company {

    public double getTotalCost(){                                    // total cost for all ( project + salary )
        return Project.getCost() + EmployeeInfo.getSalary();
    }
}
