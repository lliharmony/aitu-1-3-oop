package kz.aitu.oop.practice.practice3;
import java.util.ArrayList;
import java.util.List;

public class Employees {

    public List<EmployeeInfo> employee;

    public Employees(){
        this.employee = new ArrayList<>();
    }

    public int getNumberOfEmployees(){            //Number of employees
        return employee.size();
    }
    public int getEmployeeSalary(){         // EmployeeSalary
        return EmployeeInfo.getSalary();
    }
    public void addEmployee(EmployeeInfo e){       //addEmployee
        employee.add(e);
    }

    @Override
    public String toString() {
        return "\n"+"Employee: "+ employee ;
    }
}
