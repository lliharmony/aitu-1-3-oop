package kz.aitu.oop.practice.practice3;

import java.sql.*;

public class Main {
    public static void main(String[] args) {
        System.out.println("*************Practice task3*************");
        System.out.println(" ");
        System.out.println("*************BY USING DATABASE**************");
        String url = "jdbc:mysql://localhost:3306/practice3";
        String username = "Yerkebulan";
        String password = "1234567890";
        try {
            Connection conn = DriverManager.getConnection(url, username, password);
            Statement statement = conn.createStatement();
            ResultSet result = statement.executeQuery("SELECT* FROM employee;");
            while (result.next()) {
                System.out.println("Who will participate: "+result.getString(2));
                /*   System.out.println(TOTAL WEIGHT);*/
            }
        }
        catch (SQLException e) {
            e.printStackTrace();
        }


        try {
            Connection conn = DriverManager.getConnection(url, username, password);
            Statement statement = conn.createStatement();
            ResultSet result = statement.executeQuery("SELECT salary+sum(cost) FROM employee INNER JOIN project on employee.final_id = project.final_id");
            while (result.next()) {
                System.out.println("Total Cost : "+result.getString(1)+"KZT");
                /*   System.out.println(TOTAL WEIGHT);*/
            }
        }
        catch (SQLException e) {
            e.printStackTrace();
        }



        System.out.println(" ");
        System.out.println("**********WITHOUT USING DATABASE***********");
        EmployeeInfo em1 = new EmployeeInfo(1,"Yerkebulan","Data Scientist",15000);
        EmployeeInfo em2 = new EmployeeInfo(2,"Olki","Tutor",100000);
        EmployeeInfo em3 = new EmployeeInfo(3, "Bakytzhan","IT-Designer",29900);

        Employees empl = new Employees();
        empl.addEmployee(em1);
        empl.addEmployee(em2);
        empl.addEmployee(em3);
        empl.toString();
        empl.getNumberOfEmployees();
        empl.getEmployeeSalary();


        Project p = new Project(1,580000);
        p.getCost();

        Company com = new Company();
        com.getTotalCost();

        FinalProject finalp = new FinalProject();
        finalp.displayTotalNumberOfEmployeesToCarryOut(empl, com);
        System.out.println("****************************************");
    }
}