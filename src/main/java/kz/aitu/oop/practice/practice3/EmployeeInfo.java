package kz.aitu.oop.practice.practice3;

import lombok.Data;

@Data
public class EmployeeInfo {
    private int id;
    public String name;
    public String position;
    public static int salary;

    public EmployeeInfo(int id, String name, String position, int salary) {
        this.id = id;
        this.name = name;
        this.position = position;
        EmployeeInfo.salary = salary;
    }

    public static int getSalary() {
        return salary;
    }
}
