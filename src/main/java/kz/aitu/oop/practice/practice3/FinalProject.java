package kz.aitu.oop.practice.practice3;

public class FinalProject {

    public void displayTotalNumberOfEmployeesToCarryOut(Employees employees, Company total){
        System.out.println("Number of employees to carry out the project: " + employees.getNumberOfEmployees());
        System.out.println("Who will work in this project: " + employees.toString());
        System.out.println("Total cost for salary of employees and for project: " + total.getTotalCost()+"KZT");
    }
}
