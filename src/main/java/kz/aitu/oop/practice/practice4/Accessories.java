package kz.aitu.oop.practice.practice4;


import java.util.ArrayList;
import java.util.List;

public class Accessories {

    public List<EachAccessory> accessory;

    public Accessories() {
        this.accessory = new ArrayList<>();
    }

    public int getNumberOfAccessories() {            //Number of accessories
        return accessory.size();
    }

    public void addAccesory(EachAccessory accessoryy) {       //addAccessory
        accessory.add(accessoryy);
    }

    @Override
    public String toString() {         //getAccessory
        return "Accessory = " + accessory;
    }
}

