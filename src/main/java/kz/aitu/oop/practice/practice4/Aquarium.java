package kz.aitu.oop.practice.practice4;

public class Aquarium {
    public void getTotalCost() {
        // total cost for all ( fishes + reptiles + accessories)
        System.out.println(" ");
        System.out.println("Total price: ");
        System.out.println(EachAccessory.getAccessoryCost()+EachFish.getFishCost()+EachReptile.getReptileCost());
    }
}