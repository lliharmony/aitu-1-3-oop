package kz.aitu.oop.practice.practice4;

import lombok.Data;

@Data
public class EachFish {
    private int id;
    public static double cost;

    public EachFish(int id, double cost) {
        this.id = id;
        this.cost = cost;
    }
    public static double getFishCost(){
        return cost;
    }
}
