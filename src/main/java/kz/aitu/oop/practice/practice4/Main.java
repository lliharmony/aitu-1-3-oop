package kz.aitu.oop.practice.practice4;

import java.sql.*;

public class Main {

    public static void main(String[] args) {
        System.out.println("*************Practice task4*************");
        System.out.println(" ");
        System.out.println("*************BY USING SQL************");
        String url = "jdbc:mysql://localhost:3306/practice4";
        String username = "Yerkebulan";
        String password = "1234567890";
        try {
            Connection conn = DriverManager.getConnection(url, username, password);
            Statement statement = conn.createStatement();
            ResultSet result = statement.executeQuery("SELECT sum(distinct accessory.cost)+ sum(distinct fishes.cost) + sum(distinct reptiles.cost) FROM accessory INNER JOIN aquarium ON accessory.total_id = aquarium.total_id INNER JOIN fishes ON aquarium.total_id = fishes.total_id INNER JOIN reptiles ON aquarium.total_id = reptiles.total_id\n ");
            while (result.next()) {
                System.out.println("Total Price: "+result.getInt(1)+"KZT");
            }
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        System.out.println(" ");
        System.out.println("*************WITHOUT USING SQL*************");
            EachFish f1 = new EachFish(1, 12312);
            EachFish f2 = new EachFish(2, 23234);

            EachReptile r1 = new EachReptile(1, 20000);
            EachReptile r2 = new EachReptile(2, 24000);

            EachAccessory a1 = new EachAccessory(1,"Rakushka", 20000);
            EachAccessory a2 = new EachAccessory(2, "Domik",24000);

            Fishes fishes = new Fishes();
            fishes.addFish(f1);
            fishes.addFish(f2);
            fishes.toString();
            fishes.getNumberOfFishes();

            Reptiles reptiles = new Reptiles();
            reptiles.addReptiles(r1);
            reptiles.addReptiles(r2);
            reptiles.toString();
            reptiles.getNumberOfReptiles();

            Accessories accessory = new Accessories();
            accessory.addAccesory(a1);
            accessory.addAccesory(a2);
            accessory.getNumberOfAccessories();

            Aquarium aqua = new Aquarium();

            DisplayAquarium display = new DisplayAquarium();
            display.displayTotalNumberOfAnimals(fishes, reptiles, aqua, accessory);

            aqua.getTotalCost();

        System.out.println("****************************************");

    }
}
