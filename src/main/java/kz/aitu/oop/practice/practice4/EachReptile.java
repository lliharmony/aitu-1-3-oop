package kz.aitu.oop.practice.practice4;

import lombok.Data;

@Data
public class EachReptile {
    private int id;
    public static double cost;

    public EachReptile(int id, double cost) {
        this.id = id;
        EachReptile.cost = cost;
    }
    public static double getReptileCost(){
        return cost;
    }
}
