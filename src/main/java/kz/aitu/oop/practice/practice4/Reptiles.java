package kz.aitu.oop.practice.practice4;

import java.util.ArrayList;
import java.util.List;

public class Reptiles {

    public List<EachReptile> reptile;

    public Reptiles() {
        this.reptile = new ArrayList<>();
    }

    public int getNumberOfReptiles() {            //Number of reptiles
        return reptile.size();
    }

    public void addReptiles(EachReptile reeeeeptile) {       //addReptile
        reptile.add(reeeeeptile);
    }

    @Override
    public String toString() {         //getReptile
        return "reptile = " + reptile;
    }
}
