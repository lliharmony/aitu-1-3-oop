package kz.aitu.oop.practice.practice4;

public class DisplayAquarium {
    public void displayTotalNumberOfAnimals(Fishes fishes, Reptiles reptiles,Aquarium total, Accessories accessories){
        System.out.println("Number of fishes in aquarium: " + fishes.getNumberOfFishes());
        System.out.println("Number of reptiles in aquarium: " + reptiles.getNumberOfReptiles());
        System.out.println("Number of accessories in aquarium: " + accessories.getNumberOfAccessories());
        System.out.println("Accessors: " + accessories.toString());
        System.out.println("Fishes: " + fishes.toString());
        System.out.println("Reptiles: " + reptiles.toString());

    }
}
