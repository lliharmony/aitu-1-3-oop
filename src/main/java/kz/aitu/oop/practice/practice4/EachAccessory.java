package kz.aitu.oop.practice.practice4;

import lombok.Data;

@Data
public class EachAccessory {
    private int id;
    public String name;
    public static double cost;

    public EachAccessory(int id, String name, double cost) {
        this.id = id;
        this.name = name;
        EachReptile.cost = cost;
    }
    public static double getAccessoryCost(){
        return cost;
    }
}
