package kz.aitu.oop.practice.practice4;

import java.util.ArrayList;
import java.util.List;

public class Fishes {

    public List<EachFish> fish;

    public Fishes(){
        this.fish = new ArrayList<>();
    }

    public int getNumberOfFishes(){            //Number of fishes
        return fish.size();
    }

    public void addFish(EachFish fiiiish){       //addFish
        fish.add(fiiiish);
    }

    @Override
    public String toString() {         //getFish
        return "fish = " + fish;
    }
}