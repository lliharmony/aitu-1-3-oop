package kz.aitu.oop.practice.practice2;

import lombok.Data;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

@Data
public class RailwayCarriage {
    private int id;
    public static int railwayWeight;
    public List<Passenger> passengers;

    public RailwayCarriage(){
        passengers = new ArrayList<>();
    }

    public RailwayCarriage(int id, int railwayWeight) {
        this.id = id;
        this.railwayWeight = railwayWeight;
    }

    public void addPassenger(Passenger passenger){       //addPassenger
        passengers.add(passenger);
    }
    public Passenger getPassengers(String name, String surname){       //getPassenger
        for (Passenger passenger: passengers){
            if (passenger.getName().equals(name) && passenger.getSurname().equals(surname))
                return passenger;
        }
        return null;
    }
    public int getNumberOfPassengers(){
        return passengers.size();
    }
    public static int getRailwayWeight(){
        return railwayWeight;
    }
}