package kz.aitu.oop.practice.practice2;

import lombok.Data;

@Data
public class Locomotive {
    private int id;
    public static int locomotiveWeight;

    public Locomotive(int id, int locomotiveWeight) {
        this.id = id;
        this.locomotiveWeight = locomotiveWeight;
    }

    public static int getLocomotiveWeight(){
        return locomotiveWeight;
    }
}
