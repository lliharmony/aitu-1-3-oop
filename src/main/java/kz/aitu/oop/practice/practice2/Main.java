package kz.aitu.oop.practice.practice2;

import java.sql.*;

public class Main {
    public static void main(String[] args) {
        System.out.println("*************Practice task2*************");
        System.out.println("*************BY USING DATABASE**************");

        String url = "jdbc:mysql://localhost:3306/practice2";
        String username = "Yerkebulan";
        String password = "1234567890";
        try {
            Connection conn = DriverManager.getConnection(url, username, password);
            Statement statement = conn.createStatement();
            ResultSet result = statement.executeQuery("SELECT count(distinct id) as Number_Of_Passenger from passenger;");
            while (result.next()) {
                System.out.println("Number of passengers: "+result.getInt(1));
                /*   System.out.println(TOTAL WEIGHT);*/
            }
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        try {
            Connection conn = DriverManager.getConnection(url, username, password);
            Statement statement = conn.createStatement();
            ResultSet result = statement.executeQuery(
                    "SELECT weight+sum(railway_weight) FROM locomotive INNER JOIN railwaycarriage on locomotive.train_id = railwaycarriage.train_id");
            while (result.next()) {
                System.out.println("Total capacity of the train: "+result.getInt(1)+"kg");
                /*   System.out.println(TOTAL WEIGHT);*/
            }
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        System.out.println(" ");
        System.out.println("*************WITHOUT USING DATABASE****************");
        Passenger passenger = new Passenger(1,"Yerkebulan","Pushpakov");
        Passenger passenger1 = new Passenger(2,"Olki","Agai");

        RailwayCarriage raaail = new RailwayCarriage();
        raaail.addPassenger(passenger);
        raaail.addPassenger(passenger1);
        raaail.getPassengers();
        raaail.getNumberOfPassengers();

        RailwayCarriage railway = new RailwayCarriage(1,122200);
        railway.getRailwayWeight();

        Locomotive loco = new Locomotive(1,122400);
        loco.getLocomotiveWeight();

        Train train = new Train();
        train.TotalWeight();

        WholeTrain t = new WholeTrain();
        t.displayTotalNumberPassengers(raaail, train);
        System.out.println("****************************************");
    }
}
