package kz.aitu.oop.practice.practice2;

import lombok.Data;

@Data
public class Passenger {
    public int id;
    public String name;
    public String surname;

    public Passenger(int id, String name, String surname) {
        this.id = id;
        this.name = name;
        this.surname = surname;
    }
}
