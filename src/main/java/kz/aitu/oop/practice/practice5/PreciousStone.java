package kz.aitu.oop.practice.practice5;

import java.util.ArrayList;
import java.util.List;

public class PreciousStone {

    public List<EachPreciousStone> precious;

    public PreciousStone(){
        this.precious = new ArrayList<>();
    }

    public void addPreciousStone(EachPreciousStone eachprecious){       //addPrecious
        precious.add(eachprecious);
    }

    @Override
    public String toString() {         //getPrecious
        return "PreciousStone = " + precious;
    }
}