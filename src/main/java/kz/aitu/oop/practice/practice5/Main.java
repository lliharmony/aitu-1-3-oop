package kz.aitu.oop.practice.practice5;

import java.sql.*;

public class Main {
    public static void main(String[] args) {
        System.out.println("*************Practice task5*************");
        System.out.println(" ");
        System.out.println("*************BY USING SQL************");
        String url = "jdbc:mysql://localhost:3306/practice5";
        String username = "Yerkebulan";
        String password = "1234567890";
        try {
            Connection conn = DriverManager.getConnection(url, username, password);
            Statement statement = conn.createStatement();
            ResultSet result = statement.executeQuery("SELECT sum(semi_cost), sum(semi_weight) FROM semipreciousstone INNER JOIN necklace on semipreciousstone.total_id = necklace.total_id where necklace.total_id=4;\n");
            while (result.next()) {
                System.out.println("Cost for necklace with only SemiPreciousStone: " + result.getInt(1)+" KZT" + ", Weight: " + result.getInt(2)+"carats");
            }
        } catch (
                SQLException e) {
            e.printStackTrace();
        }

        try {
            Connection conn = DriverManager.getConnection(url, username, password);
            Statement statement = conn.createStatement();
            ResultSet result = statement.executeQuery("SELECT sum(distinct preciousCost), sum(distinct weight) FROM preciousstone INNER JOIN necklace on preciousstone.total_id = necklace.total_id where necklace.total_id=1;\n ");
            while (result.next()) {
                System.out.println("Cost for necklace with only PreciousStone: " + result.getInt(1)+" KZT" + ", Weight: " + result.getInt(2)+"carats");
            }
        } catch (
                SQLException e) {
            e.printStackTrace();
        }
        try {
            Connection conn = DriverManager.getConnection(url, username, password);
            Statement statement = conn.createStatement();
            ResultSet result = statement.executeQuery("SELECT sum(distinct preciousCost) + sum(distinct semi_cost), sum(distinct weight)+sum(distinct semi_weight) FROM preciousstone INNER JOIN necklace on preciousstone.total_id = necklace.total_id INNER JOIN semipreciousstone on semipreciousstone.total_id = necklace.total_id;\n ");
            while (result.next()) {
                System.out.println("Cost for necklace with both: " + result.getInt(1)+" KZT" + ", Weight: " + result.getInt(2)+"carats");
            }
        } catch (
                SQLException e) {
            e.printStackTrace();
        }
        System.out.println(" ");
        try {
            Connection conn = DriverManager.getConnection(url, username, password);
            Statement statement = conn.createStatement();
            ResultSet result = statement.executeQuery("SELECT sum(distinct preciousCost) + sum(distinct semi_cost), sum(distinct weight) + sum(distinct semi_weight) FROM preciousstone INNER JOIN necklace on preciousstone.total_id = necklace.total_id INNER JOIN semipreciousstone on semipreciousstone.total_id = necklace.total_id ");
            while (result.next()) {
                System.out.println("Total Cost: " + result.getInt(1)+" KZT" + ", Total Weight: " + result.getInt(2)+"carats");
            }
        } catch (
                SQLException e) {
            e.printStackTrace();
        }

        System.out.println("******************WITHOUT USING SQL***************");

        EachPreciousStone eps = new EachPreciousStone(1,16516540,"Diamond",2);
        EachPreciousStone eps1 = new EachPreciousStone(2,14815130,"Ruby",3);
        EachPreciousStone eps2 = new EachPreciousStone(3,12345679,"Brilliant",7);

        EachSemiPreciousStone ess = new EachSemiPreciousStone(1,8000000,"Moonstone",1);
        EachSemiPreciousStone ess1 = new EachSemiPreciousStone(2,7500000,"Peridot",2);
        EachSemiPreciousStone ess2 = new EachSemiPreciousStone(3,11000000,"Garnet",4);

        PreciousStone ps = new PreciousStone();
        ps.addPreciousStone(eps);
        ps.addPreciousStone(eps1);
        ps.addPreciousStone(eps2);

        SemiPreciousStone ss = new SemiPreciousStone();
        ss.addSemiPreciousStone(ess);
        ss.addSemiPreciousStone(ess1);
        ss.addSemiPreciousStone(ess2);

        Stone st = new Stone();
        st.getTotalWeight();
        st.getTotalCost();

        Necklace neck = new Necklace();
        neck.displayNecklace(st, st);

    }
}
