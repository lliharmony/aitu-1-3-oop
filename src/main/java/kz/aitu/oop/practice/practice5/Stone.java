package kz.aitu.oop.practice.practice5;

public class Stone {
    public int getTotalWeight() {
        return EachPreciousStone.getPrecious_weight()+EachSemiPreciousStone.getSemi_weight();
    }
    public double getTotalCost(){
        return EachPreciousStone.getPrecious_cost()+EachSemiPreciousStone.getSemi_cost();
    }
}

