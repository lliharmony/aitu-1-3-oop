package kz.aitu.oop.practice.practice5;

import lombok.Data;

@Data
public class EachSemiPreciousStone {
    private int id;
    public static double semi_cost;
    public String name;
    public static int semi_weight;

    public EachSemiPreciousStone(int id, double semi_cost,String name, int semi_weight){
        this.id = id;
        this.name = name;
        EachSemiPreciousStone.semi_cost = semi_cost;
        EachSemiPreciousStone.semi_weight = semi_weight;
    }

    public static double getSemi_cost() {
        return semi_cost;
    }

    public static int getSemi_weight() {
        return semi_weight;
    }


}
