package kz.aitu.oop.practice.practice5;

import java.util.ArrayList;
import java.util.List;

public class SemiPreciousStone {

    public List<EachSemiPreciousStone> semiprecious;

    public SemiPreciousStone(){
        this.semiprecious = new ArrayList<>();
    }

    public void addSemiPreciousStone(EachSemiPreciousStone semi){       //addSemiPrecious
        semiprecious.add(semi);
    }

    @Override
    public String toString() {         //getSemiPrecious
        return "SemiPrecious = " + semiprecious;
    }
}