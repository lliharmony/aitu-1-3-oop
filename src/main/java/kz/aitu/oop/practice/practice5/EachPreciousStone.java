package kz.aitu.oop.practice.practice5;

import lombok.Data;

@Data
public class EachPreciousStone {
    private int id;
    public static double precious_cost;
    public String name;
    public static int precious_weight;

    public EachPreciousStone(int id,double precious_cost,String name, int precious_weight){
        this.id = id;
        this.name = name;
        EachPreciousStone.precious_cost = precious_cost;
        EachPreciousStone.precious_weight = precious_weight;
    }

    public static double getPrecious_cost() {
        return precious_cost;
    }

    public static int getPrecious_weight() {
        return precious_weight;
    }
}
